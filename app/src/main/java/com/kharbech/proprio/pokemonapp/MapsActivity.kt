package com.kharbech.proprio.pokemonapp

import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.widget.Toast

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        loadPokemons()
        checkPermissions()
    }

    val AccesLocation =123
    fun checkPermissions() {

        if(Build.VERSION.SDK_INT>= 23){
            if (ActivityCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)!=PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),AccesLocation)
                return
            }
        }
        getUserlocation()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            AccesLocation->{
                if (grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    getUserlocation()
                } else {
                    Toast.makeText(this, "Location access denied", Toast.LENGTH_LONG).show()
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    fun getUserlocation() {

        Toast.makeText(this, "Location access now", Toast.LENGTH_LONG).show()

        val myLoc=MyLocationListener()
        val locationManager=getSystemService(Context.LOCATION_SERVICE) as LocationManager
        //the locationmanager will update the location every 3 min and 3 meters
      //  locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,3,3f,locationManager)
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3,3f, myLoc)

        val myThread=MyThread()
        myThread.start()

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap


    }


    var myLocation: Location? = null
    // this class updates the location and puts it in myLocation variable
    inner class MyLocationListener:LocationListener {

        constructor(){
            myLocation = Location("me")
            myLocation!!.longitude =0.0
            myLocation!!.latitude =0.0
        }

        // read location
        override fun onLocationChanged(location: Location?) {
            myLocation=location
        }

        // change from enable to disable
        override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
      //      TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }
        override fun onProviderEnabled(p0: String?) {
       //     TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }
        override fun onProviderDisabled(p0: String?) {
      //      TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

    }

    var oldLocation:Location?=null

    inner class MyThread:Thread{
        constructor():super(){
            oldLocation = Location("oldLocation")
            oldLocation!!.longitude =0.0
            oldLocation!!.latitude =0.0
        }

        override fun run() {
            while (true){
                try {
                    if (oldLocation!!.distanceTo(myLocation)==0f) {
                        continue
                    }

                runOnUiThread {
                   mMap!!.clear()
                    // Add a marker in Sydney and move the camera
                    val sydney = LatLng(myLocation!!.latitude, myLocation!!.longitude)
                    mMap!!.addMarker(MarkerOptions()
                            .position(sydney)
                            .title("Me")
                            .snippet("Here is my location")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.mario)))
                    mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,14f))

                    // show pokemons
                    for (i in 0..listOfPokemons.size-1) {
                        var newPokemon = listOfPokemons[i]
                        if (newPokemon.isCatched == false) {
                            val pokLocation = LatLng(newPokemon.location!!.latitude, newPokemon.location!!.longitude)
                            mMap!!.addMarker(MarkerOptions()
                                    .position(pokLocation)
                                    .title(newPokemon.name)
                                    .snippet(newPokemon.desc+ ", power : "+ newPokemon.power)
                                    .icon(BitmapDescriptorFactory.fromResource(newPokemon.image!!)))

                         //   Toast.makeText(applicationContext, "hello", Toast.LENGTH_LONG).show()

                            if (myLocation!!.distanceTo(newPokemon.location)<2) {
                                myPover=myPover+newPokemon.power!!
                                newPokemon.isCatched = true
                                listOfPokemons[i]=newPokemon
                                Toast.makeText(applicationContext, "You catched a new pokemon, your new power is : " + myPover,
                                        Toast.LENGTH_LONG ).show()
                            }
                        }
                    }
                }


                    Thread.sleep(1000)
                }catch (ex:Exception){

                }


            }

        }
    }

    var myPover:Double =0.0
    var listOfPokemons=ArrayList<Pokemon>()
    fun loadPokemons() {
        listOfPokemons.add(Pokemon(R.drawable.dratini, "dratini","dratini living in Japan",
                55.0, 35.662842, 139.829378))
        listOfPokemons.add(Pokemon(R.drawable.pikachu, "pikachu","pikachu living in Canada",
                90.5, 35.652939, 139.839477))
        listOfPokemons.add(Pokemon(R.drawable.psyduck, "psyduck","psyduck living in Tunisia",
                86.3, 35.642721, 139.819563))
    }
}

