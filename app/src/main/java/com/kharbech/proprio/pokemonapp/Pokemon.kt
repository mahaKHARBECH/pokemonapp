package com.kharbech.proprio.pokemonapp

import android.location.Location

class Pokemon {
    var name:String?=null
    var desc:String?=null
    var image:Int?=null
    var power:Double?=null
    var location:Location?=null
    var isCatched:Boolean?=false


    constructor(image:Int,name:String,desc:String, power:Double, lat:Double, long:Double) {
        this.image = image
        this.name = name
        this.desc = desc
        this.power = power
        this.location = Location(name)
        this.location!!.latitude = lat
        this.location!!.longitude = long
    }

}